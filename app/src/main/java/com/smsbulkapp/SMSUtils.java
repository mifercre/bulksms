package com.smsbulkapp;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

public class SMSUtils extends BroadcastReceiver {

    public static final String SENT_SMS_ACTION_NAME = "SMS_SENT";
    public static final String DELIVERED_SMS_ACTION_NAME = "SMS_DELIVERED";

    @Override
    public void onReceive(Context context, Intent intent) {
        //Detect l'envoie de sms
        if (intent.getAction().equals(SENT_SMS_ACTION_NAME)) {
            switch (getResultCode()) {
                case Activity.RESULT_OK: // Sms sent
                    Log.d("SMS sent result ok","");
                    Toast.makeText(context, context.getString(R.string.sms_send), Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE: // generic failure
                    Log.d("SMS not sent error","");
                    Toast.makeText(context, context.getString(R.string.sms_not_send), Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE: // No service
                    Log.d("SMS not sent no phone service","");
                    Toast.makeText(context, context.getString(R.string.sms_not_send_no_service), Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU: // null pdu
                    Log.d("SMS not sent error","");
                    Toast.makeText(context, context.getString(R.string.sms_not_send), Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF: //Radio off
                    Log.d("SMS not sent no radio service","");
                    Toast.makeText(context, context.getString(R.string.sms_not_send_no_radio), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
        //detect la reception d'un sms
        else if (intent.getAction().equals(DELIVERED_SMS_ACTION_NAME)) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Log.d("SMS received","");
                    Toast.makeText(context, context.getString(R.string.sms_receive), Toast.LENGTH_SHORT).show();
                    break;
                case Activity.RESULT_CANCELED:
                    Log.d("SMS not received","");
                    Toast.makeText(context, context.getString(R.string.sms_not_receive), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }

    /**
     * Test if device can send SMS
     * @param context
     * @return
     */
    public static boolean canSendSMS(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
    }

    public static String sendSMS(final Context context, String phoneNumber, String message) {

        if (!canSendSMS(context)) {
            Toast.makeText(context, "PHONE CAN NOT SEND SMS", Toast.LENGTH_SHORT).show();
            return "PHONE CAN NOT SEND SMS";
        }

        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, new Intent(SENT_SMS_ACTION_NAME), 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0, new Intent(DELIVERED_SMS_ACTION_NAME), 0);

        final SMSUtils smsUtils = new SMSUtils();
        //register for sending and delivery
        context.registerReceiver(smsUtils, new IntentFilter(SMSUtils.SENT_SMS_ACTION_NAME));
        context.registerReceiver(smsUtils, new IntentFilter(DELIVERED_SMS_ACTION_NAME));

        SmsManager sms = SmsManager.getDefault();
        ArrayList<String> parts = sms.divideMessage(message);
        Log.d("SMS PARTS", parts.size() + "");
        int [] data = SmsMessage.calculateLength(message, true);
        Log.d("sms required ->", data[0] + "");

        ArrayList<PendingIntent> sendList = new ArrayList<PendingIntent>();
        sendList.add(sentPI);

        ArrayList<PendingIntent> deliverList = new ArrayList<PendingIntent>();
        deliverList.add(deliveredPI);

        if(data[0] == 1) {
            sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
        }

        //we unsubscribed in 10 seconds
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                context.unregisterReceiver(smsUtils);
            }
        }, 10000);

        return "SENT_OK - Sms, to phone: " + phoneNumber;
    }
}
