package com.smsbulkapp;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Vibrator;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;


public class MainActivity extends Activity implements View.OnClickListener {

    private NotificationManager mNotificationManager;
    private Notification.Builder mNotifyBuilder;
    private int numNotifications = 0;
    private int notifyID = 1;

    private RelativeLayout smsSendingStatusBar;
    private TextView smsSendingTextView;

    private Button searchFileButton;
    private EditText pathFileEdit;

    private EditText startFromEdit;
    private EditText startToEdit;

    private EditText secondsFromDelayEdit;
    private EditText secondsToDelayEdit;
    private TextView secondsDelayText;

    private EditText smsTextEdit;
    private TextView smsTextLength;

    private static Button smsSendButton;
    private static Button smsStopSending;

    private ArrayList<String> phoneNumbers;
    private int phoneNumbersCount;
    private StringBuffer smsLogs;

    private static ArrayList<Handler> handlers = new ArrayList<Handler>();
    private static ArrayList<Runnable> runnables = new ArrayList<Runnable>();

    private SharedPreferences preferences = null;

    private TextView lastPathText;
    private TextView lastSMSText;
    private TextView lastDelayFromText;
    private TextView lastDelayToText;
    private TextView lastPhoneNoText;
    private TextView lastTotalSentText;
    private TextView lastTotalReceivedText;
    private TextView lastTotalNotReceivedText;
    private TextView lastiText;

    private int smsTotalReceived;
    private int smsTotalNotReceived;
    private int smsTotalSent;

    private int startFrom;
    private int startTo;
    //private ArrayList<String> test = new ArrayList<String>();

    public static String logFilePath;
    public static final String SENT_SMS_ACTION_NAME = "SMS_SENT";
    public static final String DELIVERED_SMS_ACTION_NAME = "SMS_DELIVERED";

    private ArrayList<MyReceiver> myBroadcastReceivers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        /**test.add("635428290");
        test.add("638510972");
        test.add("685122263");
        test.add("679929426");
        test.add("649336657");**/

        smsSendingStatusBar = (RelativeLayout) findViewById(R.id.smsSendingStatusBar);
        smsSendingTextView = (TextView) findViewById(R.id.smsSendingStatusText);

        searchFileButton = (Button) findViewById(R.id.button_load_file);
        searchFileButton.setOnClickListener(this);

        pathFileEdit = (EditText) findViewById(R.id.edit_file_path);

        startFromEdit = (EditText) findViewById(R.id.edit_start_from);
        startToEdit = (EditText) findViewById(R.id.edit_start_to);

        secondsDelayText = (TextView) findViewById(R.id.text_sms_delay);
        secondsFromDelayEdit = (EditText) findViewById(R.id.edit_from_sms_seconds_delay);
        secondsFromDelayEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                String secondsFromString = secondsFromDelayEdit.getText().toString();
                String secondsToString = secondsToDelayEdit.getText().toString();
                if (!secondsToString.isEmpty() && !secondsFromString.isEmpty()) {
                    int secondsFromDelay = Integer.parseInt(secondsFromString);
                    int secondsToDelay = Integer.parseInt(secondsToString);
                    secondsDelayText.setText(secondsFromDelay + "~" + secondsToDelay + " seconds delay");
                }
            }
        });
        secondsToDelayEdit = (EditText) findViewById(R.id.edit_to_sms_seconds_delay);
        secondsToDelayEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                String secondsFromString = secondsFromDelayEdit.getText().toString();
                String secondsToString = secondsToDelayEdit.getText().toString();
                if (!secondsToString.isEmpty() && !secondsFromString.isEmpty()) {
                    int secondsFromDelay = Integer.parseInt(secondsFromString);
                    int secondsToDelay = Integer.parseInt(secondsToString);
                    secondsDelayText.setText(secondsFromDelay + "~" + secondsToDelay + " seconds delay");
                }
            }
        });

        smsTextLength = (TextView) findViewById(R.id.text_sms_char_count);
        smsTextEdit = (EditText) findViewById(R.id.edit_sms_text);
        smsTextEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                smsTextLength.setText(smsTextEdit.getText().toString().length() + " chars");
            }
        });

        smsSendButton = (Button) findViewById(R.id.button_send_sms);
        smsSendButton.setOnClickListener(this);

        smsStopSending = (Button) findViewById(R.id.button_stop);
        smsStopSending.setOnClickListener(this);
        smsStopSending.setEnabled(false);

        lastPathText = (TextView) findViewById(R.id.text_last_path);
        lastDelayFromText = (TextView) findViewById(R.id.text_last_delay_from);
        lastDelayToText = (TextView) findViewById(R.id.text_last_delay_to);
        lastiText = (TextView) findViewById(R.id.text_last_i);
        lastPhoneNoText = (TextView) findViewById(R.id.text_last_phone);
        lastSMSText = (TextView) findViewById(R.id.text_last_sms_text);
        lastTotalSentText = (TextView) findViewById(R.id.text_last_total_sent);
        lastTotalReceivedText = (TextView) findViewById(R.id.text_last_total_received);
        lastTotalNotReceivedText = (TextView) findViewById(R.id.text_last_total_not_received);

        loadLastExecutionInfo();
    }

    private void loadLastExecutionInfo() {
        preferences = GlobalState.getSharedPreferences();
        String lastPath = preferences.getString("FilePath", "");
        String lastSMS = preferences.getString("SMSText", "");
        String lastPhone = preferences.getString("LastPhoneNo", "");
        int lastFromDelay = preferences.getInt("DelayFrom", -1);
        int lastToDelay = preferences.getInt("DelayTo", -1);
        int lastTotalPhones = preferences.getInt("TotalPhones", -1);
        int lasti = preferences.getInt("LastSent", -1);
        int lastTotalSent = preferences.getInt("SmsTotalCount", -1);
        int lastTotalReceived = preferences.getInt("LastSmsReceivedCount", 0);
        int lastTotalNotReceived = preferences.getInt("LastSmsNotReceivedCount", 0);

        if(!lastPath.isEmpty()) {
            pathFileEdit.setText(lastPath);
            lastPathText.setText("File Path: " + lastPath);
        }
        if(!lastSMS.isEmpty()) {
            smsTextEdit.setText(lastSMS);
            lastSMSText.setText("Text: " + lastSMS);
        }
        if(!lastPhone.isEmpty()) lastPhoneNoText.setText("Last Phone: " + lastPhone);
        if(lastFromDelay > 0) {
            secondsFromDelayEdit.setText(lastFromDelay + "");
            lastDelayFromText.setText("Delay between: " + lastFromDelay);
        }
        if(lastToDelay > 0) {
            secondsToDelayEdit.setText(lastToDelay + "");
            lastDelayToText.setText("Delay to: " + lastToDelay);
        }
        if(lastTotalPhones > 0 && lasti > 0) {
            startFromEdit.setText((lasti+1)+"");
            startToEdit.setText(lastTotalPhones+"");
            lastiText.setText("Phone " + lasti + " out of " + lastTotalPhones);
        }
        if(lastTotalSent > 0) {
            lastTotalSentText.setText("Total sms sent: " + lastTotalSent);
        }
        lastTotalReceivedText.setText("Total sms received: " + lastTotalReceived);
        lastTotalNotReceivedText.setText("Total sms not received: " + lastTotalNotReceived);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_load_file:
                loadFileFromStorage();
                break;
            case R.id.button_send_sms:
                if(checkForm()) {
                    sendAllSMS();
                    smsSendButton.setEnabled(false);
                    smsStopSending.setEnabled(true);
                } else {
                    Toast.makeText(this, "Error in form parameters, SMS NOT SENDING YET", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.button_stop:
                smsSendButton.setEnabled(true);
                smsStopSending.setEnabled(false);
                stopExecution();
                break;
        }
    }

    private boolean checkForm() {
        String sms = smsTextEdit.getText().toString();
        String delay = secondsFromDelayEdit.getText().toString();
        if(sms != null && !sms.isEmpty() && delay != null && !delay.isEmpty() && phoneNumbers != null && !phoneNumbers.isEmpty()) return true;
        else return false;
    }

    public static void stopExecution() {
        if(smsSendButton != null && smsStopSending != null) {
            smsSendButton.setEnabled(true);
            smsStopSending.setEnabled(false);
        }
        for(int i=0; i<handlers.size(); i++) {
            handlers.get(i).removeCallbacks(runnables.get(i));
        }
    }

    public void onButtonChooseFile(View v) {
        //Create FileOpenDialog and register a callback
        SimpleFileDialog fileOpenDialog =  new SimpleFileDialog(
                MainActivity.this,
                "FileOpen..",
                new SimpleFileDialog.SimpleFileDialogListener()
                {
                    @Override
                    public void onChosenDir(String chosenDir)
                    {
                        // The code in this function will be executed when the dialog OK button is pushed
                        if(chosenDir != null && chosenDir.startsWith("//")) chosenDir = chosenDir.substring(1, chosenDir.length());
                        pathFileEdit.setText(chosenDir);
                    }
                }
        );
        //You can change the default filename using the public variable "Default_File_Name"
        fileOpenDialog.default_file_name = pathFileEdit.getText().toString();
        fileOpenDialog.chooseFile_or_Dir(fileOpenDialog.default_file_name);
    }

    private void sendAllSMS() {

        myBroadcastReceivers = new ArrayList<MyReceiver>();

        writeHeaderToLogFile();

        smsSendingStatusBar.setVisibility(View.VISIBLE);

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);

        mNotifyBuilder = new Notification.Builder(this)
                .setContentTitle("SMS Bulk")
                .setContentText("Starting to send")
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentIntent(pIntent);

        mNotificationManager.notify(
                notifyID,
                mNotifyBuilder.build());

        Log.d("Button clicked", "Send SMS");
        smsLogs = new StringBuffer();

        int secondsFromDelay = Integer.parseInt(secondsFromDelayEdit.getText().toString());
        int secondsToDelay = Integer.parseInt(secondsToDelayEdit.getText().toString());
        final String sms = smsTextEdit.getText().toString();

        saveExecutionInfo(pathFileEdit.getText().toString(), sms, secondsFromDelay, secondsToDelay, phoneNumbers.size());

        startFrom = Integer.parseInt(startFromEdit.getText().toString());
        startTo = Integer.parseInt(startToEdit.getText().toString());

        int time=0;

        Log.d("Start from", startFrom + "");
        Log.d("Start to", startTo + "");

        Random r = new Random();
        int currentRand = 0;
        int previousRand = 0;
        if(startFrom <= startTo && startFrom <= phoneNumbers.size() && startTo <= phoneNumbers.size()) {
            for(int i=startFrom; i<startTo; i++) {

                final String phone = phoneNumbers.get(i);
                //final String phone = test.get(i);
                final int count = i;

                Runnable mMyRunnable = new Runnable() {
                    @Override
                    public void run() {
                        //smsLogs.append(SMSUtils.sendSMS(getApplicationContext(), phone, sms) + "\n");
                        //smsLogs.append(sendSMSMessage(sms, phone, count) + "\n");
                        sendSMSMessage(sms, phone, count);
                    }
                };
                previousRand += currentRand;
                currentRand = r.nextInt(secondsToDelay-secondsFromDelay) + secondsFromDelay;

                int delay = currentRand + previousRand;
                Handler myHandler = new Handler();
                myHandler.postDelayed(mMyRunnable, 1000 * delay); //Message will be delivered in x second.

                handlers.add(myHandler);
                runnables.add(mMyRunnable);
                Log.d("Handler", time + myHandler.toString() + ", Runnable ->" + mMyRunnable.toString());
                Log.d("FOR LOOP", "Iteration ->" + time + ", Count ->" + i + ", Handlers size ->" + handlers.size());
                time += 1;
            }
        }
        else {
            Toast.makeText(this, "Parameters not setted correctly", Toast.LENGTH_SHORT).show();
        }
    }

    private void saveExecutionInfo(String path, String smsText, int delayFrom, int delayTo, int totalPhones) {
        preferences = GlobalState.getSharedPreferences();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("FilePath", path);
        editor.putString("SMSText", smsText);
        editor.putInt("DelayFrom", delayFrom);
        editor.putInt("DelayTo", delayTo);
        editor.putInt("TotalPhones", totalPhones);
        editor.commit();
    }

    private void loadFileFromStorage() {
        File sdcard = Environment.getExternalStorageDirectory();

        String pathToFile = pathFileEdit.getText().toString();
        Log.d("Trying to load", pathToFile);

        //Get the text file
        File file = new File(pathToFile);

        //Read text from file
        phoneNumbers = new ArrayList<String>();
        phoneNumbersCount = 0;
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                phoneNumbers.add(line);
                phoneNumbersCount += 1;
            }
            br.close();
        }
        catch (IOException e) {
            //You'll need to add proper error handling here
            Toast.makeText(this, "File not found", Toast.LENGTH_SHORT).show();
        }

        //Find the view by its id
        TextView tv = (TextView)findViewById(R.id.text_view);

        //Set the text
        tv.setText("Find " + phoneNumbersCount + " phone numbers");
        startToEdit.setText(phoneNumbersCount + "");
        //startFromEdit.setText(0 + "");
    }

    protected String sendSMSMessage(String message, String phoneNo, int i) {
        Log.i("Send SMS", "Sms " + (i+1) + ", to phone: " + phoneNo);
        smsTotalSent += 1;

        SharedPreferences preferences = GlobalState.getSharedPreferences();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("SmsTotalCount", smsTotalSent);
        editor.commit();

        try {

            if(smsSendingTextView != null && smsSendingStatusBar != null) {
                if((i+1) == startTo) {
                    smsSendingStatusBar.setBackgroundColor(Color.BLUE);
                    smsSendingTextView.setText("ALL SMS SENT - msg " + (i+1)  + " out of " + startTo);
                } else {
                    smsSendingTextView.setText("Sending msg " + (i+1)  + " out of " + startTo);
                }
            }

            PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(SENT_SMS_ACTION_NAME), 0);
            PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, new Intent(DELIVERED_SMS_ACTION_NAME), 0);

            final MyReceiver broadcastReceiver = new MyReceiver((i+1), phoneNo);
            //register for sending and delivery
            //this.getApplicationContext().registerReceiver(broadcastReceiver, new IntentFilter(SMSUtils.SENT_SMS_ACTION_NAME));
            this.getApplicationContext().registerReceiver(broadcastReceiver, new IntentFilter(DELIVERED_SMS_ACTION_NAME));

            //myBroadcastReceivers.add(broadcastReceiver);

            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, message, sentPI, deliveredPI);
            //SMSUtils.sendSMS(getApplicationContext(), phoneNo, message);

            //we unsubscribed in 10 seconds
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        if(broadcastReceiver != null && broadcastReceiver.isOrderedBroadcast()) {
                            getApplicationContext().unregisterReceiver(broadcastReceiver);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 10000);

            Toast.makeText(getApplicationContext(), "SMS " + (i+1) + " sent.", Toast.LENGTH_SHORT).show();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentTimeStamp = dateFormat.format(new Date());

            String logBody = "Date: " + currentTimeStamp + " SENT_OK - Sms " + (i+1) + ", to phone: " + phoneNo;
            writeToLogFile(logBody, phoneNo, (i+1));
            updateNotification((i+1), startTo);

            return "Date: " + currentTimeStamp + "SENT_OK - Sms " + (i+1) + ", to phone: " + phoneNo;
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "SMS failed, please try again.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            return "ERROR - Sms " + (i+1) + ", to phone: " + phoneNo;
        }
    }

    private void updateNotification(int i, int startTo) {
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if(i == startTo) {
            mNotifyBuilder.setContentText("ALL SMS SENT - sms " + i + " out of " + startTo)
                    .setNumber(++numNotifications);
            Vibrator v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
            // Vibrate for 500 milliseconds
            v.vibrate(4000);
        } else {
            mNotifyBuilder.setContentText("Sending sms " + i + " out of " + startTo)
                    .setNumber(++numNotifications);
        }

        mNotificationManager.notify(
                notifyID,
                mNotifyBuilder.build());
    }

    private void writeHeaderToLogFile() {
        try {
            String path = pathFileEdit.getText().toString();
            path = path.substring(0, path.lastIndexOf("/")) + "/sms_spam_logs.txt";
            logFilePath = path;

            Log.d("PATH FILE OUT", path);
            File logFile = new File(path);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentTimeStamp = dateFormat.format(new Date());

            BufferedWriter bW = new BufferedWriter(new FileWriter(logFile, true));

            bW.append("\n\n --------------------------------------- \n");
            bW.append("STARTING NEW EXECUTION\n");
            bW.append("CURRENT DATE: " + currentTimeStamp + "\n");

            bW.flush();
            bW.close();
            //Toast.makeText(mContext, "Tus datos han sido guardados", Toast.LENGTH_SHORT).show();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

    private void writeToLogFile(String logBody, String phoneNo, int i) {
        try {
            String path = pathFileEdit.getText().toString();
            path = path.substring(0, path.lastIndexOf("/")) + "/sms_spam_logs.txt";

            Log.d("PATH FILE OUT", path);
            File logFile = new File(path);

            BufferedWriter bW = new BufferedWriter(new FileWriter(logFile, true));
            bW.append(logBody + "\n");

            bW.flush();
            bW.close();

            preferences = GlobalState.getSharedPreferences();
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("LastSent", i);
            editor.putString("LastPhoneNo", phoneNo);
            editor.commit();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        smsSendingStatusBar = (RelativeLayout) findViewById(R.id.smsSendingStatusBar);
        smsSendingTextView = (TextView) findViewById(R.id.smsSendingStatusText);

        if(handlers != null && !handlers.isEmpty()) {
            smsSendingStatusBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);

        Notification n  = new Notification.Builder(this)
                .setContentTitle("SMS bulk destroyed")
                .setContentText("Not running")
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentIntent(pIntent)
                .setAutoCancel(true).build();

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        notificationManager.notify(0, n);
    }

    public class MyReceiver extends BroadcastReceiver {

        private int errorCounter = 0;

        private int phoneCount = 0;
        private String phoneNumber = "";
        private boolean alreadyReceived = false;

        public MyReceiver() {
            super();
        }

        public MyReceiver(int count, String phone) {
            super();
            phoneCount = count;
            phoneNumber = phone;
            alreadyReceived = false;
        }

        public void error(Context context) {
            Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            errorCounter +=1;
            if(errorCounter >= 7) {
                v.vibrate(4000);
                stopExecution();

                mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                mNotifyBuilder.setContentText("YOU MAY HAVE BEEN BANNED!!!").setNumber(++numNotifications);

                Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.error_128);
                mNotifyBuilder.setLargeIcon(bm);
                mNotificationManager.notify(
                        notifyID,
                        mNotifyBuilder.build());
            }
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("BroadcastReceiver", "onReceive");
            if(!alreadyReceived) {
                Log.d("BroadcastReceiver", "alreadyReceived = false, " + intent.getAction());
                this.alreadyReceived = true;
                //Detect l'envoie de sms
                if (intent.getAction().equals(SMSUtils.SENT_SMS_ACTION_NAME)) {
                    Log.d("BroadcastReceiver", "SENT_SMS_ACTION_NAME");
                    switch (getResultCode()) {
                        case Activity.RESULT_OK: // Sms sent
                            Log.d("SMS sent result ok", context.getString(R.string.sms_send) + ", count: " + phoneCount + ", phone: " + phoneNumber);
                            errorCounter = 0;
                            Toast.makeText(context, context.getString(R.string.sms_send) + ", count: " + phoneCount + ", phone: " + phoneNumber, Toast.LENGTH_SHORT).show();
                            break;
                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE: // generic failure
                            Log.d("SMS not sent error", context.getString(R.string.sms_not_send) + ", count: " + phoneCount + ", phone: " + phoneNumber);
                            error(context);
                            Toast.makeText(context, context.getString(R.string.sms_not_send) + ", count: " + phoneCount + ", phone: " + phoneNumber, Toast.LENGTH_SHORT).show();
                            break;
                        case SmsManager.RESULT_ERROR_NO_SERVICE: // No service
                            Log.d("No phone service", context.getString(R.string.sms_not_send_no_service) + ", count: " + phoneCount + ", phone: " + phoneNumber);
                            error(context);
                            Toast.makeText(context, context.getString(R.string.sms_not_send_no_service) + ", count: " + phoneCount + ", phone: " + phoneNumber, Toast.LENGTH_SHORT).show();
                            break;
                        case SmsManager.RESULT_ERROR_NULL_PDU: // null pdu
                            Log.d("SMS not sent error", context.getString(R.string.sms_not_send) + ", count: " + phoneCount + ", phone: " + phoneNumber);
                            error(context);
                            Toast.makeText(context, context.getString(R.string.sms_not_send) + ", count: " + phoneCount + ", phone: " + phoneNumber, Toast.LENGTH_SHORT).show();
                            break;
                        case SmsManager.RESULT_ERROR_RADIO_OFF: //Radio off
                            Log.d("No radio service", context.getString(R.string.sms_not_send_no_radio) + ", count: " + phoneCount + ", phone: " + phoneNumber);
                            error(context);
                            Toast.makeText(context, context.getString(R.string.sms_not_send_no_radio) + ", count: " + phoneCount + ", phone: " + phoneNumber, Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
                //detect la reception d'un sms
                else if (intent.getAction().equals(DELIVERED_SMS_ACTION_NAME)) {
                    Log.d("BroadcastReceiver", "DELIVERED_SMS_ACTION_NAME");
                    switch (getResultCode()) {
                        case Activity.RESULT_OK:
                            Log.d("BroadcastReceiver", "RESULT_OK");
                            SharedPreferences preferences1 = GlobalState.getSharedPreferences();
                            SharedPreferences.Editor editor1 = preferences1.edit();
                            //int previous1 = preferences1.getInt("LastSmsReceivedCount", 0);
                            smsTotalReceived += 1;
                            editor1.putInt("LastSmsReceivedCount", smsTotalReceived);
                            editor1.commit();

                            Log.d("SMS received", context.getString(R.string.sms_receive) + ", count: " + phoneCount + ", phone: " + phoneNumber + ", total: " + smsTotalReceived);
                            Toast.makeText(context, context.getString(R.string.sms_receive) + ", count: " + phoneCount + ", phone: " + phoneNumber, Toast.LENGTH_SHORT).show();
                            break;
                        case Activity.RESULT_CANCELED:
                            Log.d("BroadcastReceiver", "RESULT_CANCELED");
                            SharedPreferences preferences2 = GlobalState.getSharedPreferences();
                            SharedPreferences.Editor editor2 = preferences2.edit();
                            //int previous2 = preferences2.getInt("LastSmsNotReceivedCount", 0);
                            smsTotalNotReceived += 1;
                            editor2.putInt("LastSmsNotReceivedCount", smsTotalNotReceived);
                            editor2.commit();

                            Log.d("SMS not received", context.getString(R.string.sms_not_receive) + ", count: " + phoneCount + ", phone: " + phoneNumber + ", total: " + smsTotalNotReceived);
                            Toast.makeText(context, context.getString(R.string.sms_not_receive) + ", count: " + phoneCount + ", phone: " + phoneNumber, Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }

        }
    }
}
